package ru.vmaksimenkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.util.TerminalUtil;

public final class ProjectByIdStartCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Start project by id";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-start-by-id";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[START PROJECT]");
        System.out.println("ENTER ID:");
        serviceLocator.getProjectService().startProjectById(userId, TerminalUtil.nextLine());
    }

}
