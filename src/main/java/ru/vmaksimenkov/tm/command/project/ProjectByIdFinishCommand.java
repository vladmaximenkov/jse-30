package ru.vmaksimenkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.exception.entity.NoProjectsException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

public final class ProjectByIdFinishCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Finish project by id";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-finish-by-id";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        if (serviceLocator.getProjectService().size(userId) < 1) throw new NoProjectsException();
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER ID:");
        serviceLocator.getProjectService().finishProjectById(userId, TerminalUtil.nextLine());
    }

}
