package ru.vmaksimenkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.exception.entity.ProjectNotFoundException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectByIdSetStatusCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Set project status by id";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-set-status-by-id";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SET PROJECT STATUS]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        if (!serviceLocator.getProjectService().existsById(userId, id)) throw new ProjectNotFoundException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        serviceLocator.getProjectService().setProjectStatusById(userId, id, Status.getStatus(TerminalUtil.nextLine()));
    }

}
