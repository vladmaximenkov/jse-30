package ru.vmaksimenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.util.TerminalUtil;

public final class TaskByIdFinishCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Finish task by id";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-finish-by-id";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER ID:");
        serviceLocator.getTaskService().finishTaskById(userId, TerminalUtil.nextLine());
    }

}
