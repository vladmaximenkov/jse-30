package ru.vmaksimenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.model.Task;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.System.out;

public final class TaskListCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show task list";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-list";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        out.println("[TASK LIST]");
        out.printf("\t| %-36s | %-12s | %-20s | %-30s | %-30s | %-30s | %s %n", "ID", "STATUS", "NAME", "CREATED", "STARTED", "FINISHED", "PROJECT");
        @Nullable final List<Task> list = serviceLocator.getTaskService().findAll(userId);
        if (list == null) return;
        @NotNull AtomicInteger index = new AtomicInteger(1);
        list.forEach((x) -> out.println(index.getAndIncrement() + "\t" + x));
    }

}
