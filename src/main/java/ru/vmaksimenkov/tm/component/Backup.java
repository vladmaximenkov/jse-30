package ru.vmaksimenkov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.api.service.IPropertyService;
import ru.vmaksimenkov.tm.bootstrap.Bootstrap;
import ru.vmaksimenkov.tm.command.data.BackupLoadCommand;
import ru.vmaksimenkov.tm.command.data.BackupSaveCommand;

public class Backup extends Thread {

    @NotNull
    private final Bootstrap bootstrap;
    private final int interval;

    public Backup(@NotNull final Bootstrap bootstrap, @NotNull final IPropertyService propertyService) {
        this.bootstrap = bootstrap;
        this.interval = propertyService.getBackupInterval();
        setDaemon(true);
    }

    public void init() {
        load();
        start();
    }

    public void load() {
        bootstrap.parseCommand(BackupLoadCommand.NAME);
    }

    @Override
    @SneakyThrows
    public void run() {
        //noinspection InfiniteLoopStatement
        while (true) {
            save();
            //noinspection BusyWait
            Thread.sleep(interval);
        }
    }

    public void save() {
        bootstrap.parseCommand(BackupSaveCommand.NAME);
    }

}
