package ru.vmaksimenkov.tm.exception.entity;

public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException() {
        super("Error! Entity not found...");
    }

}
